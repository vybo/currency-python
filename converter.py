import sys
import argsparser
import json
import urllib2
import symbolconverter


opts = argsparser.parseArgs(sys.argv[1:])

# This is quick and ugly bugfix
if "--output_currency" in opts.keys():
    opts["--output_currency"] = symbolconverter.toCode(opts["--output_currency"])

try:
    input = {}
    input["currency"] = symbolconverter.toCode(opts["--input_currency"])
    input["amount"] = opts["--amount"];
    response = urllib2.urlopen("http://api.fixer.io/latest?base=" + input["currency"])

    ratesString = response.read()
    ratesDict = json.loads(ratesString)

    output = {}

    if "error" in ratesDict.keys():
        print "Api Error:" + ratesDict["error"]
        sys.exit(0);

    elif "--output_currency" in opts.keys() and opts["--output_currency"] not in str(ratesDict["rates"].items()):
        print "Api Error: The API does not provide conversion rates for the specified output currency" \
              " or the output currency was improperly specified."
        sys.exit(0);

    elif "--output_currency" in opts.keys():
        output[opts["--output_currency"]] = ratesDict["rates"][opts["--output_currency"]] * float(input["amount"])

    else:
        for key in ratesDict["rates"]:
            output[key] = ratesDict["rates"][key] * float(input["amount"])

    result = {}
    result["input"] = input
    result["output"] = output
    print json.dumps(result, indent=4, sort_keys=True)

except urllib2.HTTPError, e:
    print('HTTPError = ' + str(e.code))
except urllib2.URLError, e:
    print('URLError = ' + str(e.reason))
except NameError:
    print "Variable undefined."
except Exception:
    import traceback
    print('generic exception: ' + traceback.format_exc())